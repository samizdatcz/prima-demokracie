title: "Více přímé demokracie: čeká nás polarizace společnosti a oslabení parlamentu?"
perex: "V letošních parlamentních volbách zabodovaly strany, které požadují více přímé demokracie. Chtějí ji Okamura, komunisté i Piráti, ANO se jí nebrání. Jaké nástroje použít, aby se veřejnost zapojila do politiky – a ne proti politice?"
authors: ["Jan Boček"]
published: "15. listopadu 2017"
coverimg: https://www.irozhlas.cz/sites/default/files/uploader/landsgemeinde_trogen_171114-143022_jab.jpg
coverimg_note: "Johann Jakob Mock: Die Landsgemeinde in Trogen | <a href='https://commons.wikimedia.org/wiki/File:Landsgemeinde_Trogen_1814.jpg'>Wikimedia Commons</a>"
styles: []
libraries: ["https://unpkg.com/jquery@3.2.1", "https://code.highcharts.com/highcharts.js"]
options: "" #wide
---

Zákonodárná iniciativa občanů, lidové veto, přímá volba starostů, odvolatelnost politiků, zákon o obecném referendu, hlasování o vystoupení z Evropské unie. Sliby politických stran před parlamentními volbami byly plné termínů, spojovaných s přímou demokracií.

<wide>TABULKA S PŘEDVOLEBNÍMI SLIBY</wide>

Poptávka po přímé demokracii není nová. Po volbách má ovšem poprvé reálnou šanci se prosadit. Přímá demokracie se nabízí protestním stranám jako silné téma, které dosud vládnoucí politici ignorovali.

„Česká republika patří mezi pouhé tři země v Evropské unii, jejichž právní řád neobsahuje žádná pravidla pro konání referend,“ potvrzuje český deficit ústavní právník Marek Antoš v loni vydaném sborníku [Přímá demokracie](https://knihy.abz.cz/prodej/prima-demokracie).

Ústava přitom vznik zákona o obecném referendu – tedy pravidel pro uspořádání všelidového hlasování – předpokládá už v původním znění z prosince 1992. „Ústavní zákon může stanovit, kdy lid vykonává státní moc přímo,“ [píše se v Ústavě](https://www.zakonyprolidi.cz/cs/1993-1#cl2). Takový ústavní zákon se ovšem za 25 let existence státu přijmout nepodařilo. Bez něj je možné vyvolat celostátní referendum pouze přijetím zvláštního ústavního zákona, tak jako při hlasování o vstupu do Evropské unie.

„Výhrada ústavního zákona, který je nutný i pro případné konání referenda ad hoc, představuje zřejmě vůbec nejrestriktivnější přístup k přímé demokracii v rámci zemí EU,“ dodává Antoš.

Situace bez zákonné úpravy referenda je nejen neobvyklá, ale zřejmě také neudržitelná. Nástroje přímé demokracie posilují jak v tradičních demokraciích, tak v čerstvě demokratizovaných zemích včetně postkomunistických.

„Mám za to, že institut referenda zažívá pomyslný boom, a to i v těch zemích, kde bylo využíváno velmi málo, nebo dokonce vůbec,“ píše ve zmíněné publikaci Přímá demokracie ústavní soudce Vojtěch Šimíček. Zmiňuje nástup referend v Nizozemsku, Řecku, Spojeném království, Irsku, Španělsku, a také – s výhradou – připojení Krymu k Ruské federaci.

„Silnější hlas veřejnosti může být slibný lék na současnou krizi demokracie,“ tvrdí britský politolog Matt Qvortrup v knize [Referendums Around the World: The Continued Growth of Direct Democracy](http://www.palgrave.com/us/book/9780230361751) z roku 2014. Posilování přímé demokracie popisuje Qvortrup už od sedmdesátých let minulého století. Nástroje přímé demokracie podle něj můžou pomoci „upustit páru“ ve společnosti; zvlášť tam, kde nastolování témat ovládly silné politické strany – nebo to tak veřejnost vnímá.

Ohnisko přímé demokracie v unii představují země východní Evropy: Pobaltí a země Visegrádu – Maďarsko a Slovensko.

Jenže jak ukazují příklady evropských zemí – a nejsilněji právě Slovenska – přímá demokracie má řadu parametrů, které při špatném nastavení mohou politický život spíš komplikovat.

<div id="kapitoly">
<h3>Přeskočte na kapitolu, která vás zajímá</h3>
<ul>
<li><a href="#k1">Historie přijímání obecného referenda: 35 marných pokusů</a></li>
<li><a href="#k2">Proč nepřijmout slovenskou úpravu</a></li>
<li><a href="#k3">Typy referend: je snazší bořit a ničit?</a></li>
<li><a href="#k4">Švýcarsko: polovina celosvětových referend a klesající účast</a></li>
<li><a href="#k5">Proč raději ne?</a></li>
<li><a href="#k6">Obrat: bude nejskeptičtější země nejotevřenější k přímé demokracii?</a></li>
</ul>
</div>

<div id="k1"></div>
## Historie přijímání obecného referenda: 35 marných pokusů

Ne že by se politici nesnažili. Od první Klausovy do zatím poslední Sobotkovy vlády se pokusili přijmout úpravu celostátního referenda 35krát, pokaždé ovšem neúspěšně.

Analýza Marka Antoše ukazuje, kdo se snažil nejvíc: podle něj zákon nejčastěji navrhovali komunisté a sociální demokraté, často společně. Třikrát Úsvit. Pětkrát vycházel z dílny vlády, 27krát šlo o poslanecký návrh, třikrát vzešel ze Senátu.

Výjimkou není ani vláda premiéra Sobotky. Její [návrh zákona o obecním referendu](http://www.psp.cz/sqw/text/tiskt.sqw?O=7&CT=559&CT1=0) z rukou ministra pro lidská práva Jiřího Dienstbiera ovšem ve sněmovně před dvěma lety po několikahodinové diskusi skončil v prvním čtení.

<right>
BOX K OBECNÍMU REFERENDU
dovafv
vfsgdibs
</right>

Řada právníků se shoduje, že je to dobře. Vládní návrh kopíroval, místy doslovně, slovenskou úpravu referenda. Zkušenosti ze Slovenska přitom ukazují na řadu problémů; z devíti referend bylo platné pouze jedno, které vyžaduje ústava: hlasování o vstupu do Evropské unie. U ostatních referend nepřekročila účast padesát procent voličů, hlasování tedy nebyla závazná.

Skutečné problémy by ovšem podle právních expertů nastaly v okamžiku, kdy by se podařilo hranici pro závazné referendum překonat. Nejasnosti ohledně přesného znění a síly zákona přijatého v referendu, by zřejmě byly oříškem i pro ústavní soud. Ten se ostatně na Slovensku do výkladu podmínek referenda pravidelně zapojuje.

„Slovensko pro nás představuje unikátní ‚ústavněprávní laboratoř‘,“ vysvětluje ústavní soudce Vojtěch Šimíček. „Právě v oblasti referenda nám Slovensko může nabídnout celou řadu pozitivních, ale též negativních zkušeností a Česká republika by se měla řídit zásadou, že jen hlupák opakuje stejné chyby, kterých se již dopustil někdo jiný.“

<div id="k2"></div>
## Proč nepřijmout slovenskou úpravu

Dienstbierův návrh sice zapadl, ale slovenská inspirace se v návrzích českého zákona o referendu objevuje pravidelně. Jak zní český návrh a co je na něm špatně?

Český návrh ve zkratce říká:

- Návrh na konání referenda podává kterýkoliv plnoletý český občan; pro konání referenda musí do šesti měsíců posbírat 250 tisíc podpisů. (Na Slovensku 350 tisíc.)
- Otázka – nebo otázky – pro referendum musí mít jednoznačnou odpověď ano/ne.
- Otázka se nesmí týkat základních práv a svobod, státního rozpočtu a daní, ustanovování a odvolávání lidí do/z funkcí, individuálních práv a povinností. Nesmí také vést k porušení mezinárodních závazků.
- Návrh na referendum ověří vláda, případně Ústavní soud. Pokud je formálně v pořádku, vyhlásí prezident konání referenda. Od okamžiku, kdy vláda obdržela petici s podpisy, se tak referendum může konat nejdřív za tři a nejpozději za osm měsíců. Zákon tlačí na propojení referenda s některými nadcházejícími volbami.
- Referendum je závazné při účasti nejméně 25 procent všech voličů. (Na Slovensku je to 50 procent.)
- Pokud je referendum závazné, rozhodnutí zavazuje současnou vládu i obě komory Parlamentu. Sněmovna nemůže referendum přehlasovat po celé volební období, nejméně ale tři roky.
- Další referendum o stejné věci se nesmí konat dříve než za tři roky.

Na čerty v detailech upozorňuje právní teoretik Jan Wintr z Univerzity Karlovy. Klíčovou komplikací je podle něj fakt, že autor referenda na Slovensku navrhuje přesné znění otázky, kterou má později vláda nebo parlament přijmout v podobě zákona. V případě závazného referenda pak může nastat konflikt mezi existujícími zákony a formulací přijatou v referendu.

„Vše nasvědčuje tomu, že v případě kolize mezi zákonem a rozhodnutím přijatým v referendu má přednost stávající zákon, což je z hlediska právní jistoty spíš dobře,“ vysvětluje Wintr situaci ve slovenské „ústavní laboratoři“.

Slovensko je podle Wintra specifické tím, že navrhovatel sám formuluje otázku, která je pro parlament závazná. V řadě dalších zemí sice veřejnost iniciuje projednávání otázky, ale přesnou formulaci nechává na zákonodárcích; ti ji schvalují v běžném legislativním procesu. Snadněji tak mohou sladit otázku s platným právním řádem.

Druhý problém současného znění naznačuje slovenský ústavní soud. Ten rozhodl, že rozhodnutí přijaté v referendu sice může být závazné, ale není vymahatelné. Slovenský zákon ani český návrh ostatně nepočítají s žádnou sankcí.

Výsledek referenda totiž zavazuje instituce – vládu a parlament – ale ne jednotlivce, tedy samotné ministry, poslance a senátory. Při hlasování ovšem mohou poslanci hlasovat sami za sebe, klidně proti návrhu.

Slovenská zkušenost tedy ukazuje, že cesta vede spíš jinudy. Kudy?

<div id="k3"></div>
## Typy referend: je snazší bořit a ničit?

_Iniciativní referendum_, kdy veřejnost navrhuje přijetí zákona jako na Slovensku, je v Evropské unii poměrně neobvyklé; podle analýzy ústavního právníka Marka Antoše je využívá pouze pětice postkomunistických zemí a částečně Francie. Klíčovým problémem je, jak vynutit závaznost takového hlasování. V Litvě, Lotyšsku a ve Francii se proto hlasuje přímo o návrhu zákona. Bulharský, maďarský a slovenský zákon problém neřeší.

Pětadvacet zemí unie počítá s nějakou formou referenda, pouze tři země jsou výjimkou. Vedle Česka, které čeká na přijetí ústavního zákona, je to Belgie a Kypr. Belgie přímou demokracii odmítá také z toho důvodu, že jde o společnost se silnou dělicí linií mezi Vlámy a Valony; lidové hlasování může zažehnout závažný konflikt. Kypr umožňuje vyvolat _ad hoc_ referendum běžným zákonem.

Ve dvanácti zemích unie zákon v určitých situacích – typicky při změně ústavy, přistoupení nebo vystoupení ze státního celku, případně při uzavření mezinárodní smlouvy – stanoví povinná (_obligatorní_) referenda. Ve čtyřiadvaceti zemích může veřejnost nebo skupina poslanců vyvolat nepovinná (_fakultativní_) referenda: doporučit parlamentu téma k projednání, navrhnout nebo zrušit zákon. Unikátní je Německo, které sice vyžaduje referendum o změně hranic spolkových zemí, ale občanům možnost iniciovat jakékoliv hlasování odpírá. Reaguje tak na zkušenosti z období třicátých let, kdy Hitler pomocí referend obcházel parlamentní instituce.

<wide>TABULKA ZEMÍ A NÁSTROJŮ</wide>

Vedle iniciativního referenda slovenského typu je ve hře několik dalších variant. V zemích unie je nejobvyklejším typem _participační referendum_, kdy iniciativa vychází od úřadu, který má otázku ve své kompetenci. Tomu obvykle uspořádání referenda v určitých situacích nařizuje zákon, pak jde o již zmíněné obligatorní referendum. Někdy se ale může parlament obrátit na veřejnost dobrovolně, pokud se například necítí povolán odpovídat na určitou otázku. S participačním referendem počítá právo sedmnácti zemí unie.

Méně obvyklé je v Evropě _blokační referendum_. V sedmi zemích EU může lidové hlasování zrušit rozhodnutí, které přijal parlament. Referendum vyvolává buď skupina poslanců (Dánsko a Irsko), veřejnost (Malta, Nizozemsko a Slovinsko), nebo je třeba společná iniciativa (Itálie, Lotyšsko). Blokace se většinou týká čerstvě přijatých norem, na Maltě a v Itálii lze zrušit i starší zákon.

Výjimečným typem je _racionalizační referendum_. To umožňuje výkonné moci obcházet parlament a využívají je pouze Francie s Chorvatskem.

<div id="k4"></div>
## Švýcarsko: polovina celosvětových referend a klesající účast

Víc než polovina referend na planetě se odehraje v jediné zemi – ve Švýcarsku. Podle výzkumníka Uwe Serdülta z Curyšské univerzity Švýcaři ročně hlasují průměrně v devíti referendech na národní a v dalších pěti na regionální úrovni, v kantonech.

Švýcarská demokracie a prosperita je taky jeden z nejčastějších argumentů pro přímou demokracii. Nejen Tomio Okamura propojení přímé demokracie s vysokou životní úrovní zmiňuje pravidelně. Takhle jednoduchou závislost ovšem nelze spolehlivě prokázat a už vůbec neplatí, že by přijetí švýcarského modelu ve střední Evropě mělo vést ke stejnému výsledku.

Inspirativnější pro český politický systém může být hledání, kde se švýcarská přímá demokracie narodila. Na federální – tedy celošvýcarské – úrovni se první prvky přímé demokracie objevily v ústavě z revolučního roku 1848. Už o několik desetiletí dřív se ale lidová shromáždění a hlasování objevovala v kantonech.

Podle jedné z teorií šlo o pokračování historických tradic, podle jiné k nim přispěla napoleonská okupace země a radikálně demokratické metody francouzské revoluce. Přímá demokracie té doby umožňovala ve většině kantonů vetovat schválený zákon, odvolat parlament a hlasovat na lidovém shromáždění (_Landsgemeinde_). Na shromáždění se sešli všichni občané obce, kteří směli volit, a hlasovali zvednutím ruky. Dodnes tenhle zvyk přežil ve dvou švýcarských kantonech a v mnoha obcích.

„Fyzické násilí i další praktiky, například kupování hlasů, byly během _Landsgemeinde_ tolerovány, někdy je oficiálně schvalovaly i úřady,“ píše Serdült. „Nešlo každopádně ve své době o nic neobvyklého.“

Tři nejpoužívanější nástroje v moderním Švýcarsku a na celostátní úrovni mají svůj původ ve druhé polovině devatenáctého století. První ústava z roku 1848 uvádí mandatorní ústavní referendum, tedy povinné všelidové hlasování při každé změně ústavy. Podmínkou změny ústavy je souhlas dvojité většiny: většiny občanů i většiny kantonů.

V roce 1874 se přidalo legislativní referendum, umožňující zrušit schválený zákon. K jeho vyhlášení je třeba 50 tisíc podpisů posbíraných během 100 dní, rozhoduje prostá většina zúčastněných. Od svého zavedení se uskutečnilo 180krát, 78krát úspěšně.

Třetí důležitý prvek, lidovou iniciativu, Švýcaři zavedli v roce 1891. Pokud navrhovatel pod svůj stručně formulovaný návrh – například „zákaz exportu zbraní“ – během roku a půl posbírá 100 tisíc podpisů, federální vláda a parlament se musejí jeho návrhem zabývat. Na jeho základě mohou vypracovat návrh zákona nebo vytvořit protinávrh. Pro úspěch návrhu v následujícím referendu je opět potřeba dvojitá většina, občanů i kantonů.

Poslední prvek je nejvyužívanější. Podnětů ke změně ústavy Švýcaři dodnes podali téměř dvě stě, pouze dvaadvacet ovšem bylo úspěšných. Uspěl třeba zákaz absintu (rok 1908), přistoupení k OSN (2002) nebo zákaz stavby mešit (2009). Neúspěšný byl pokus zrušit armádu (1989), zavést šest týdnů dovolené (2012) nebo zavést základní příjem (2016, k hlasování jsme [odpovídali na nejčastější otázky](https://interaktivni.rozhlas.cz/zakladni-prijem/)). Častým tématem je imigrace: v roce 2002 švýcarská veřejnost odmítla snížit kvóty na počty imigrantů, pro snížení bylo 49,9 procenta. V roce 2014 naopak snížení kvót prošlo, 50,3 procenta voličů návrh podpořilo.

![Plakát kampaně v úspěšném referendu za zákaz absintu](https://samizdat.cz/data/prima-demokracie/texty/absint.jpg)
<div>_Plakát kampaně v úspěšném referendu za zákaz absintu_</div>

Přímá demokracie má ovšem na své občany vysoké nároky. Hlavně na čas: frekventovaná referenda na celostátní, kantonální i lokální úrovni vedou k tomu, že od druhé světové války klesla průměrná účast z 60 na 45 procent. Nejnižší účast je u starších žen; ženy totiž ve Švýcarsku získaly volební právo až v roce 1971 a řada z nich si na ně nezvykla dodnes. V posledním švýcarském kantonu si (mužští) voliči bránili výhradní volební právo až do roku 1991, kdy o jeho zrušení musel rozhodnout ústavní soud.

Právě pozdější přijímání „novinek“ je dalším typickým prvkem švýcarského systému. Vedle volebního práva pro ženy, které si Švýcaři odhlasovali jako jedni z posledních v západním světě, brání lidové veto také vstupu do mezinárodních organizací. K OSN země přistoupila až v roce 2002, do Evropské unie a řady dalších organizací se nezapojila vůbec.

Hodnotit, jaký vliv přímá demokracie na rozvoj země má, každopádně není snadné.

„Švýcarsko si dnes vede dobře v řadě ukazatelů výkonnosti,“ shrnuje Uwe Serdült. „Jestli to lze připisovat přímé demokracii, je jiná otázka. Ti, kdo jsou zklamaní pomalým vývojem v sociálních záležitostech, by zase neměli zapomínat na konzervativní povahu švýcarských voličů.“

„Referenda také nelze úplně oddělit od hlasování v parlamentu,“ uzavírá Serdült. „Jejich výsledky odrážejí vůli voličů, a to jsou stále ti stejní, ať hlasují v plebiscitu, nebo parlamentních volbách.“

<iframe width="560" height="315" src="https://www.youtube.com/embed/H3i0HTtyxXc" frameborder="0" allowfullscreen></iframe>

<div id="k5"></div>
## Proč raději ne?

V dubnu 1938, měsíc po anšlusu Rakouska, se v zemi konalo referendum. „Souhlasíte se sjednocením Rakouska s Německou říší […] a volíte kandidátku našeho vůdce Adolfa Hitlera?“ Tak zněla otázka. Oficiální výsledek: 99,73 procenta _ja_ při účasti 99,71 procenta. Grafická forma odpovědi přitom jemně napovídala, kterou variantu si zvolit.

![Hlasovací lístek v rakouském referendu o přistoupení k Německé říši](https://samizdat.cz/data/prima-demokracie/texty/anschluss.jpg)
<div>_Hlasovací lístek v rakouském referendu o přistoupení k Německé říši_</div>

Výsledky referenda přitom byly podle historiků zmanipulované jen zčásti. Spíš šlo o „výsledek oportunismu, ideologického přesvědčení, masivního tlaku, občasného falšování hlasů a propagandy v takovém rozsahu, který rakouská politická kultura nikdy dřív nezažila,“ cituje Wikipedie rakouského profesora historie Güntlera Bischofa. O tajné volbě nemohla být ani řeč, voliči zaškrtávali lístky před volebními komisaři.

Referendum o anšlusu je sice extrémní případ, ale upozorňuje, že termín přímá demokracie nemusí mít s demokracií nic společného. Obratně ho využívají k legitimizaci moci také diktátoři: rumunský komunistický prezident Ceausescu, dlouholetý syrský lídr Háfiz al-Asad nebo irácký vůdce Saddám Husajn. Ten uspořádal také nejjednoznačnější referendum všech dob. Na otázku „Souhlasíte, že by měl Saddám Husajn zůstat prezidentem?“ z října 2002 odpovědělo 100 procent voličů kladně při totální účasti.

Ústavní soudce Vojtěch Šimíček upozorňuje na rizika, nerozlučně spojená s přímou demokracií: „Zejména jde o riziko populismu a také alibismu, tedy přenesení odpovědnosti za politické rozhodování z konkrétních politických aktérů na ‚bezbřehý lid‘,“ tvrdí Šimíček. Přímá demokracie podle něj v některých případech „koliduje s reprezentativní povahou mandátu“.

Politologové dodávají, že referendum ze své podstaty zostřuje politický konflikt. Tam, kde by zastupitelská demokracie skrz vyjednávání a hledání kompromisů dokázala spory otupit, má logika ano/ne, typická pro referendum, opačný efekt. Přímá demokracie spíše posiluje trendy polarizace a radikalizace společnosti.

Svou roli může sehrát i to, kdo a s jakými limity formuluje otázku. Výsledek často záleží na jejím znění. Sugestivní detail ve formulaci může hodně napomoci k výsledku, který si navrhovatel přeje.

Současná zastupitelská demokracie je výsledek mnohaletého úsilí o vyladění pravidel tak, aby volby byly rovnou soutěží politických stran. Ne vždy se to daří, ale v případě přímé demokracie bychom se ocitli v bodě nula. Jak přesně stanovit pravidla pro kampaň? Jak zabránit tomu, aby referendum ovládly politické strany nebo úspěšní podnikatelé se svými zájmy? Jaká formulace otázky je ještě v pořádku, a co už ne? Jakou minimální volební účast – s ohledem na specifika českých voličů – vyžadovat pro závaznost referenda?

Politologové také zmiňují, že referendum je málokdy neutrální nástroj. „Více moci lidem“ sice vypadá demokraticky, ale často pomáhá k moci spíš tomu, kdo se v zastupitelské demokracii obtížně prosazuje. Ve Švýcarsku je ke svému zviditelnění často využívají malé strany nebo hnutí jednoho tématu. V Česku referendum prosazuje například KSČM nebo SPD, pro které standardní politická cesta dlouho vypadala neschůdně.

<div id="k6"></div>
## Obrat: bude nejskeptičtější země nejotevřenější k přímé demokracii?

Čtvrtstoletí po vzniku ústavy, která mluví o přímé demokracii, se zdá, že se pohnuly ledy. Šance, že se podaří přijmout zákon o obecném referendu, se po volbách prudce zvýšila. Pokud má ale přímá demokracie přispět k české politické kultuře, je třeba vybrat nástroje, které nenaruší fungování zastupitelské demokracie.

„Česká republika má šanci na pozoruhodný – až kopernikovský – obrat,“ shrnuje téma ústavní právník Marek Antoš. „Zatímco dosud je unikátní svým odtažitým přístupem k přímé demokracii, v případě schválení vládního návrhu ústavního zákona o celostátním referendu by se mocným skokem přesunula rovnou na opačný konec spektra.“

„Místo možného doplnění či rozšíření stávající reprezentativní demokracie by tedy šlo ve své podstatě o nástroj směřující proti reprezentativní demokracii, určený primárně pro aktéry, kteří chtějí na politiku působit zvenčí,“ varuje Antoš. „Po zavedení přímé volby prezidenta by tak mohlo jít o další krok oslabující parlamentní formu vlády v České republice a snižující akceschopnost vlády. Krok, který by ve svých důsledcích mohl spíše než k většímu zapojení občanů do politiky přispět k zapojení občanů proti politice.“
